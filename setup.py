from distutils.core import setup

setup(
    name='TowelStuff',
    version='0.1dev',
    description= 'Finite difference two-phase porous media steady state thermal field solver.',
    author = 'Keith King',
    author_email = 'kingkeit@msu.edu',
    packages=['porous_material_convection'],
    license='MIT',
    install_requires=[
        'numpy'
        'matplotlib'
        'jupyter'
        'matplotlib'
        'pylint'
        'pytest'
        'pdoc3'
    ]
    long_description=open('README.txt').read(),
)

