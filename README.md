---
# Program Description

This program is a solver designed to recreate the temperature distribution for a fluidized, two-dimensional porous material contained within a cylindrical geometry. The basis is an Implicit Finite Difference numerical solver for solving a coupled system of partial differential equations. Finite difference equations are discretized algebraic expressions used to represent the value of a partial differential expression at a given point in space and time. These are approximations that allow for solving problems involving partial differential equations that cannot easily be solved analytically (if at all). An implicit algorthim requires that you solve each equation at every point in the model domain simultaneously. This approach is necessary because the solid and fluid phase energy equations are coupled, particularly in the boundary conditions, and thus cannot be solved explicitly. 

This software package will solve for the thermal fields in both phases for any set of material geometries. The source code can also be easily modified to change the boundary conditions and the system-level geometry for whatever real-world system you want to model. The model is based on the volume-averaging theory of porous materials. An in-depth explanation o the theory is beyond the scope of this project and can be found online. 


---
# Install Instructions

To run this software, you'll need to create your own Conda environment and install several Python packages. The following code will install everything you'll need to run the software.

1. Create your environment in a sub-folder within the current working directory. The default Python packages are included here in the install, but more can be added by simply adding the library's name to the list of libraries seen after the "./envs" command.
    
    conda create --prefix ./envs jupyter matplotlib numpy pylint pytest pdoc3


Note: The only requirement to run the software is NumPy. All of the data and equations are matrix-based, so NumPy is absolutely critical for doing anything with this software. However, it will also be quite useful to install MatPlotLib and Jupyter for viewing results, manipulating the program, and just generally doing any sort of analysis using this software. The PyLint, PyTest, and PDoc3 modules are also included just in case you want to make a lot of changes to the source code so that you can easily test, debug, and re-document your changes for your own purposes.

2. Activate your environment.

    conda activate ./envs

3. When done using the software, deactivate and delete your environment.
    
    conda deactivate
    rm -rf ./envs

Note: If you have any questions or issues with installing the environment, or would just like to go about it a different way, follow the link below to find Anaconda's instructions and guides for managing Conda enviroments. Link is up to date as of 4/2/20.

https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html